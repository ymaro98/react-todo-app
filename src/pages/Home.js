import React, { Component } from 'react';
import Todos from '../components/Todos';
import AddTodo from '../components/AddTodo';

class Home extends Component {

    state = {
		todos: [
			{id: 1, content: 'buy some milk'},
			{id: 2, content: 'play some ps4'},
		]
	}

	deleteTodo = (id) => {
		
		const oldTodos = this.state.todos;

		const todos = oldTodos.filter(todo => {
			return todo.id !== id;
		});

		this.setState({
			todos
		})
	}

	addTodo = (todo) => {
		let new_max_id = this.getMaxId();
		
		todo.id = new_max_id;
		let todos = [...this.state.todos, todo];

		this.setState({
			todos
		})
	}

	getMaxId = () => {

		const todos = this.state.todos;
		let max = Math.max.apply(Math, todos.map(function(o) { return o.id; }))

		return Number(max) + 1;
    } 

    render(){

        
        return (
            <div className="container">
                <h1 className="center blue-text">Todo's</h1>
                <Todos todos={this.state.todos} deleteTodo={this.deleteTodo}/>
                <AddTodo addTodo={this.addTodo}/>
            </div>
       )
    }
}

export default Home