import React from 'react'

const About = () => {
    return (
        <div className="container">
            <h4 className="center">About</h4>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veniam id ab placeat, labore ad nostrum! Quibusdam, dignissimos dicta repudiandae itaque vero pariatur deleniti non nulla et excepturi recusandae labore odit.</p>
        </div>
    )
}

export default About